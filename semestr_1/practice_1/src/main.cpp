#include <array>
#include <ctime>
#include <forward_list>
#include <fstream>
#include <functional>
#include <random>
#include <vector>

unsigned int get_rand_number(unsigned int min, unsigned int max) {
  return std::rand() % (max - min) + min;
}

void set_id(std::string &str) {
  static const std::string ALPHABET{"ABCDEFGHIJKLMNOPQRSTUVWXYZ" // [0; 26)
                                    "abcdefghijklmnopqrstuvwxyz" // [26; 52)
                                    "0123456789_"};              // [52; 64)

  // str.at(0) = ALPHABET.at(get_rand_number(0u, 26u));
  for (unsigned int idx{0}; idx < str.length(); ++idx) {
    str.at(idx) = ALPHABET.at(get_rand_number(0u, ALPHABET.length()));
  }
}

void generate(std::forward_list<std::string> &list, unsigned int minLength,
              unsigned int maxLength) {
  for (auto &id : list) {
    id.resize(get_rand_number(minLength, maxLength));
    set_id(id);
  }
}

void print(std::ostream &out, std::forward_list<std::string> &list) {
  for (auto &id : list) {
    out << id << std::endl;
  }
}

constexpr unsigned int HASH_RANGE{1000};
constexpr unsigned int NUM_HASH_TYPES{2};

unsigned int get_hash_1(std::string &str) {
  unsigned int sum{0};
  for (auto &ch : str) {
    sum += ch;
  }
  return sum % HASH_RANGE;
}

unsigned int get_hash_2(std::string &str) {
  unsigned int idx_1{static_cast<unsigned int>(str.length()) - 2};
  unsigned int idx_0{static_cast<unsigned int>(str.length()) - 1};
  return (idx_1 * str.at(idx_1) + idx_0 * str.at(idx_0)) % HASH_RANGE;
}

void print(std::ostream &out, std::vector<std::string> &array) {
  for (unsigned int idx{0}; idx < array.size(); ++idx) {
    out << idx << '\t' << array.at(idx) << std::endl;
  }
}

int main() {
  std::srand(std::time(nullptr));

  unsigned int ID_SIZE{400};
  std::forward_list<std::string> ID;
  ID.resize(ID_SIZE);
  generate(ID, 2, 9);

  std::ofstream out{"/home/sergey/projects/TGU/computer-science/semestr_1/"
                    "practice_1/log/ID.txt"};
  print(out, ID);
  out.close();

  std::array<std::function<unsigned int(std::string &)>, NUM_HASH_TYPES>
      get_hash{get_hash_1, get_hash_2};

  std::array<std::vector<std::string>, NUM_HASH_TYPES> M_ID;
  for (auto &m_id : M_ID) {
    m_id.resize(HASH_RANGE);
  }

  std::array<std::vector<std::string>, NUM_HASH_TYPES> M_Col;

  for (unsigned int hashType = 0; hashType < NUM_HASH_TYPES; ++hashType) {
    std::function<unsigned int(std::string &)> currHashAlg{
        get_hash.at(hashType)};
    std::vector<std::string> *currM_ID{&M_ID.at(hashType)};
    std::vector<std::string> *currM_Col{&M_Col.at(hashType)};

    for (auto &id : ID) {
      unsigned int hash{currHashAlg(id)};
      if (currM_ID->at(hash).empty()) {
        currM_ID->at(hash) = id;
      } else {
        currM_ID->at(hash) += '\t' + id;
        currM_Col->push_back(std::to_string(hash) + '\t' + currM_ID->at(hash));
      }
    }

    out.open(
        "/home/sergey/projects/TGU/computer-science/semestr_1/practice_1/log/"
        "M_ID_" +
        std::to_string(hashType) + ".txt");
    print(out, *currM_ID);
    out.close();

    out.open(
        "/home/sergey/projects/TGU/computer-science/semestr_1/practice_1/log/"
        "M_Col_" +
        std::to_string(hashType) + ".txt");
    print(out, *currM_Col);
    out << 100. * currM_Col->size() / ID_SIZE << " collision %" << std::endl;
    out.close();
  }

  return 0;
}
